'use strict';
const transports = require('./transports');

let _connectors = {};

function init(configuration) {
    _createConnectors(configuration);
}

async function send(connector, options) {
    try {
        return await _connectors[connector].send(options);
    } catch(error) {
        throw `Message for ${connector} not sent: ${error.message}`;
    }
}

function reset() {
    _connectors = {};
}

function _createConnectors(connectors) {
    if(!connectors || !connectors.length) throw new Error('Connectors list must contain at least one item');
    connectors.forEach(connector => {
        if(!Object.prototype.hasOwnProperty.call(transports, connector.transport)) throw new Error(`Unsupported transport "${connector.transport}"`);
        _connectors[connector.transport] = transports[connector.transport]();
        _connectors[connector.transport].init(connector.options);
    });
}

module.exports = {
    init,
    send,
    reset
};
