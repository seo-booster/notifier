'use strict';
const nodemailerInstance = require ('nodemailer');
const smtpTransportInstance = require ('nodemailer-smtp-transport');

function email (
  nodemailer = nodemailerInstance,
  smtpTransport = smtpTransportInstance
) {
  return {
    init: function (transport) {
      this.transporter = nodemailer.createTransport (smtpTransport (transport));
      return true;
    },
    send: async function ({ from, to, subject, text, html, attachments }) {
        return this.transporter.sendMail ({
            from, to, subject, text, html, attachments,
        });
    },
  };
}

module.exports = email;
